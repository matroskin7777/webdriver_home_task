Feature: Tasks
  As a user
  I want to test image search functionality
  So that I can be sure that it works correctly

Scenario Outline: User searches for images in google and checks that search is successful
  Given User opens '<homePage>' and search for images
  When User clicks on images button
  Then User checks that images are displaying

  Examples:
    | homePage                |
    | https://www.google.com/ |