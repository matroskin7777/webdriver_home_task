package manager;

import org.openqa.selenium.WebDriver;
import pages.*;

public class PageFactoryManager {

    WebDriver driver;

    public PageFactoryManager(WebDriver driver){
        this.driver = driver;
    }

    public StartPage getStartPage(){ return new StartPage(driver);}

    public SearchPage getSearchPage(){ return new SearchPage(driver); }

}
