package stepdefinitions;


import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.SearchPage;
import pages.StartPage;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class StepDefinitions {

    PageFactoryManager pageFactoryManager;

    WebDriver driver;

    StartPage startPage;

    SearchPage searchPage;


    @Before
    public void testsSetup()
    {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @Given("User opens {string} and search for images")
    public void openStartPage(final String url){
        startPage = pageFactoryManager.getStartPage();
        startPage.openStartPage(url);
        startPage.searchForImages("images");
    }

    @When("User clicks on images button")
    public void userClicksOnImagesButton() {
        searchPage = pageFactoryManager.getSearchPage();
        searchPage.ClickImageButton();
    }

    @Then("User checks that images are displaying")
    public void userChecksThatImagesAreDisplaying() {
        Assert.assertTrue(searchPage.AreImagesExists());
    }

    @After
    public void tearDown(){
        driver.close();
    }
}
