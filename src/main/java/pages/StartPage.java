package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.security.Key;

public class StartPage extends BasePage {

    @FindBy(xpath = "//input[@class = 'gLFyf gsfi']")
    private WebElement searchField;

    public StartPage(WebDriver driver) {
        super(driver);
    }

    public void openStartPage(final String url){
        driver.get(url);
    }

    public void searchForImages(final String searchText){
        searchField.sendKeys(searchText, Keys.ENTER);
        waitForPageLoadComplete(WAIT_TIME);
    }

}
