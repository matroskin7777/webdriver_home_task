package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchPage extends BasePage{

    @FindBy(xpath = "//a[@data-hveid = 'CAEQAw']")
    private WebElement imageButton;

    @FindBy(xpath = "//div[@jsname = 'BN6WQc']//img")
    private List<WebElement> images;

    public SearchPage(WebDriver driver) {
        super(driver);
    }

    public void ClickImageButton(){
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", imageButton);
        waitForPageLoadComplete(WAIT_TIME);
    }

    public boolean AreImagesExists(){
        return images.size() > 0;
    }

}
